﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arand
{
    class aCoin : aRandomNumberGenerator
    {
        public aCoin(int seed) : base(0,2, seed) { }
        public aCoin() : base(0, 2, 0) { }

        public override string ToString()
        {
            return "Coin";
        }
    }
}
