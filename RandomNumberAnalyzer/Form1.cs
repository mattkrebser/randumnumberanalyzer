﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using arand;

namespace histo_form
{
    public partial class main_form : Form
    {
        static void Main()
        {
            Application.Run(new main_form());
        }

        /// <summary>
        /// list of items
        /// </summary>
        List<aRandomNumberGenerator> active_generators = new List<aRandomNumberGenerator>();

        /// <summary>
        /// List of the random numbers that have been generated for the currently
        /// selected list item
        /// </summary>
        List<int> current_results = new List<int>();
        /// <summary>
        /// currently selected list item
        /// </summary>
        aRandomNumberGenerator current_active_item;

        /// <summary>
        /// The 2nd form that holds the histogram
        /// </summary>
        display_form current_display_form;

        /// <summary>
        /// entry point.
        /// </summary>
        public main_form()
        {
            InitializeComponent();
            this.list_box.DataSource = active_generators;
        }

        /// <summary>
        /// Add a new guassian to the list of random items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void guassian_click(object sender, EventArgs e)
        {
            int seed_value = 0;
            aGuassian new_rand;
            if (String.IsNullOrWhiteSpace(seed_box.Text))
            {
                new_rand = new aGuassian();
                active_generators.Add(new_rand);
                list_box.DataSource = null; list_box.DataSource = active_generators;
            }
            else if (Int32.TryParse(this.seed_box.Text, out seed_value))
            {
                new_rand = new aGuassian(seed_value);
                active_generators.Add(new_rand);
                list_box.DataSource = null; list_box.DataSource = active_generators;
            }
            else
            {
                MessageBox.Show("Please input an intiger number as a seed value.");
            }
        }

        /// <summary>
        /// A a new dice to the list of random items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void die_click(object sender, EventArgs e)
        {
            int seed_value = 0;
            aDie new_rand;
            if (String.IsNullOrWhiteSpace(seed_box.Text))
            {
                new_rand = new aDie();
                active_generators.Add(new_rand);
                list_box.DataSource = null; list_box.DataSource = active_generators;
            }
            else if (Int32.TryParse(this.seed_box.Text, out seed_value))
            {
                new_rand = new aDie(seed_value);
                active_generators.Add(new_rand);
                list_box.DataSource = null; list_box.DataSource = active_generators;
            }
            else
            {
                MessageBox.Show("Please input an intiger number as a seed value.");
            }
        }

        /// <summary>
        /// Action to add a coin the list of random items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void coin_click(object sender, EventArgs e)
        {
            int seed_value = 0;
            aCoin new_rand;
            if (String.IsNullOrWhiteSpace(seed_box.Text))
            {
                new_rand = new aCoin();
                active_generators.Add(new_rand);
                list_box.DataSource = null; list_box.DataSource = active_generators;
            }
            else if (Int32.TryParse(this.seed_box.Text, out seed_value))
            {
                new_rand = new aCoin(seed_value);
                active_generators.Add(new_rand);
                list_box.DataSource = null; list_box.DataSource = active_generators;
            }
            else
            {
                MessageBox.Show("Please input an intiger number as a seed value.");
            }
        }

        /// <summary>
        /// Stop generating numbers, and stop re-drawing the chart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stop_click(object sender, EventArgs e)
        {
            if (application_tick.Enabled)
            {
                application_tick.Stop();
                new_rand_tick.Stop();
                stopButton.Text = "Start";
            }
            else
            {
                application_tick.Start();
                new_rand_tick.Start();
                stopButton.Text = "STOP";
            }
        }

        /// <summary>
        /// Display a histogram for the item in the list that gest clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void list_action(object sender, EventArgs e)
        {
            if (list_box.SelectedItem != null)
            {
                current_display_form?.Close();
                current_active_item = null;
                current_results.Clear();
                current_active_item = (aRandomNumberGenerator)((ListBox)sender).SelectedItem;

                if (current_active_item != null)
                {
                    if (!application_tick.Enabled)
                    {
                        application_tick.Start();
                        new_rand_tick.Start();
                        stopButton.Text = "STOP";
                    }

                    current_display_form = new display_form();
                    current_display_form.Show();
                }
            }
        }

        /// <summary>
        /// Add 20 new random numbers to ou random number list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void new_rand_gen(object sender, EventArgs e)
        {
            if (current_active_item != null)
            {
                //do 20 generations every tick.
                for (int i = 0; i < 20; i++)
                {
                    current_results.Add(current_active_item.Next());
                }
            }
        }

        /// <summary>
        /// Re-Draw the chart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void on_update(object sender, EventArgs e)
        {
            if (current_results != null && current_results.Count > 0 &&
                !current_display_form.Disposing && !current_display_form.IsDisposed)
                current_display_form?.chart_update(current_results);
        }
    }
}
