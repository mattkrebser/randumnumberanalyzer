﻿namespace histo_form
{
    partial class main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.list_box = new System.Windows.Forms.ListBox();
            this.seed_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.aGaussianButton = new System.Windows.Forms.Button();
            this.aDieButton = new System.Windows.Forms.Button();
            this.aCoinButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.application_tick = new System.Windows.Forms.Timer(this.components);
            this.new_rand_tick = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // list_box
            // 
            this.list_box.FormattingEnabled = true;
            this.list_box.Location = new System.Drawing.Point(12, 12);
            this.list_box.Name = "list_box";
            this.list_box.Size = new System.Drawing.Size(186, 225);
            this.list_box.TabIndex = 0;
            this.list_box.SelectedValueChanged += new System.EventHandler(this.list_action);
            // 
            // seed_box
            // 
            this.seed_box.Location = new System.Drawing.Point(363, 30);
            this.seed_box.Name = "seed_box";
            this.seed_box.Size = new System.Drawing.Size(124, 20);
            this.seed_box.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(360, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Seed";
            // 
            // aGaussianButton
            // 
            this.aGaussianButton.Location = new System.Drawing.Point(363, 78);
            this.aGaussianButton.Name = "aGaussianButton";
            this.aGaussianButton.Size = new System.Drawing.Size(75, 23);
            this.aGaussianButton.TabIndex = 3;
            this.aGaussianButton.Text = "Guassian";
            this.aGaussianButton.UseVisualStyleBackColor = true;
            this.aGaussianButton.Click += new System.EventHandler(this.guassian_click);
            // 
            // aDieButton
            // 
            this.aDieButton.Location = new System.Drawing.Point(363, 117);
            this.aDieButton.Name = "aDieButton";
            this.aDieButton.Size = new System.Drawing.Size(75, 23);
            this.aDieButton.TabIndex = 4;
            this.aDieButton.Text = "Dice";
            this.aDieButton.UseVisualStyleBackColor = true;
            this.aDieButton.Click += new System.EventHandler(this.die_click);
            // 
            // aCoinButton
            // 
            this.aCoinButton.Location = new System.Drawing.Point(363, 158);
            this.aCoinButton.Name = "aCoinButton";
            this.aCoinButton.Size = new System.Drawing.Size(75, 23);
            this.aCoinButton.TabIndex = 5;
            this.aCoinButton.Text = "Coin";
            this.aCoinButton.UseVisualStyleBackColor = true;
            this.aCoinButton.Click += new System.EventHandler(this.coin_click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(363, 199);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 6;
            this.stopButton.Text = "STOP";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stop_click);
            // 
            // application_tick
            // 
            this.application_tick.Enabled = true;
            this.application_tick.Interval = 200;
            this.application_tick.Tick += new System.EventHandler(this.on_update);
            // 
            // new_rand_tick
            // 
            this.new_rand_tick.Enabled = true;
            this.new_rand_tick.Interval = 1;
            this.new_rand_tick.Tick += new System.EventHandler(this.new_rand_gen);
            // 
            // main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 423);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.aCoinButton);
            this.Controls.Add(this.aDieButton);
            this.Controls.Add(this.aGaussianButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.seed_box);
            this.Controls.Add(this.list_box);
            this.Name = "main_form";
            this.Text = "Random Histogram Creator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox list_box;
        private System.Windows.Forms.TextBox seed_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button aGaussianButton;
        private System.Windows.Forms.Button aDieButton;
        private System.Windows.Forms.Button aCoinButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Timer application_tick;
        private System.Windows.Forms.Timer new_rand_tick;
    }
}

