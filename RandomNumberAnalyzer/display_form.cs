﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace histo_form
{
    public partial class display_form : Form
    {
        public display_form()
        {
            InitializeComponent();
        }

        private void display_form_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Update the chart with the new data.
        /// </summary>
        /// <param name="rand_results"></param>
        public void chart_update(List<int> rand_results)
        {
            if (histogram != null)
            {
                rand_results.Distinct().OrderByDescending(x => x).ToList()
                    .ForEach(x => histogram.Series["Random Data"].Points.
                    AddXY(x, rand_results.Where(a => a == x).Count()));
            }
        }
    }
}
