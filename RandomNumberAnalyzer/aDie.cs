﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arand
{
    class aDie : aRandomNumberGenerator
    {
        public aDie(int seed) : base(1, 7, seed) { }
        public aDie() : base(1, 7, 0) { }

        public override string ToString()
        {
            return "Die";
        }
    }
}
