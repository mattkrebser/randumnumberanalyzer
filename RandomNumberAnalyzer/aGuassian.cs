﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arand
{
    class aGuassian : aRandomNumberGenerator
    {
        public aGuassian(int seed) : base(1, 7, seed) { }
        public aGuassian() : base(1, 7, 0) { }

        public override int Next()
        {
            return base.Next() + base.Next() + base.Next() +
                base.Next() + base.Next() + base.Next();
        }

        public override string ToString()
        {
            return "Guassian";
        }
    }
}
