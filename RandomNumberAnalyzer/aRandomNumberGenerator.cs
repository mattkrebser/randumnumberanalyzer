﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arand
{
    public class aRandomNumberGenerator : System.Random
    {
        int lo, hi, my_seed;
        System.Random my_rand;

        /// <summary>
        /// Highest random value range
        /// </summary>
        public int High
        {
            get
            {
                return hi;
            }
        }

        /// <summary>
        /// lowest random value range
        /// </summary>
        public int Low
        {
            get
            {
                return lo;
            }
        }

        public aRandomNumberGenerator(int lowest, int highest, int seed)
        {
            if (seed != 0)
                my_rand = new System.Random(seed);
            else
                my_rand = new System.Random();

            lo = lowest;
            hi = highest;
            my_seed = seed;
        }

        /// <summary>
        /// Get the next random number
        /// </summary>
        /// <returns></returns>
        public override int Next()
        {
            return my_rand.Next(lo, hi);
        }

        /// <summary>
        /// Get the next random number between 0 and maxvalue
        /// </summary>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public override int Next(int maxValue)
        {
            return my_rand.Next(0, maxValue);
        }
    }
}