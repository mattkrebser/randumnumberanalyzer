# README #

Random Number Analyzer.

### What is this repository for? ###

* A simple Windows c# Form application that displays Random number generation in real time. It compares coins, dice, and Gaussian random generation patterns.

### How do I get set up? ###

* This repository is a visual studio solution.
* Download the repository and open the visual studio project solution file.
* Made in VS 2015